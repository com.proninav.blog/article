pipeline {
  agent none
  options {
    skipStagesAfterUnstable()
    skipDefaultCheckout()
  }
  environment {
    IMAGE_BASE = 'com.blog/article'
    IMAGE_TAG = "v$BUILD_NUMBER"
  	IMAGE_NAME = "${env.IMAGE_BASE}:${env.IMAGE_TAG}"
  	IMAGE_NAME_LATEST = "${env.IMAGE_BASE}:latest"
  	DOCKERFILE_NAME = "Dockerfile"
  }
  stages {
    stage("Prepare container") {
      agent {
        docker {
          image 'openjdk:11.0.5-slim'
          args '-v /home/.m2:/root/.m2'
        }
      }
      stages {
        stage('Build') {
          steps {
            checkout scm
            sh './gradle build -x test'
          }
        }
        stage('Test') {
          steps {
            sh './gradle test'
            junit '**/build/test-results/test/TEST-*.xml'
          }
        }
      }
    }

    stage('Push images') {
      agent any
      when {
        branch 'master'
      }
      steps {
        script {
          def dockerImage = docker.build("${env.IMAGE_NAME}", "-f ${env.DOCKERFILE_NAME} .")
          docker.withRegistry('', 'dockerhub-creds') {
            dockerImage.push()
            dockerImage.push("latest")
          }
          echo "Pushed Docker Image: ${env.IMAGE_NAME}"
        }
        sh "docker rmi ${env.IMAGE_NAME} ${env.IMAGE_NAME_LATEST}"
      }
    }
  }
}