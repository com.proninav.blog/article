FROM openjdk:11.0.10-jre-slim
COPY build/libs/article-*-all.jar /home/app/article.jar
ENTRYPOINT ["java", "-jar", "/home/app/article.jar"]
