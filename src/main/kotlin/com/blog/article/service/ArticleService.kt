package com.blog.article.service

import com.blog.article.repository.ArticleRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ArticleService {

    @Inject
    lateinit var articleRepository: ArticleRepository

    fun findAll() =
        articleRepository.findAll()


}