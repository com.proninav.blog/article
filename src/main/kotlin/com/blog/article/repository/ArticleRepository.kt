package com.blog.article.repository

import com.blog.article.model.Article
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.r2dbc.annotation.R2dbcRepository
import io.micronaut.data.r2dbc.repository.ReactorCrudRepository

@R2dbcRepository(dialect = Dialect.POSTGRES)
interface ArticleRepository : ReactorCrudRepository<Article, Int>