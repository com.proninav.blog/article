package com.blog.article.model

import io.micronaut.data.annotation.GeneratedValue
import io.micronaut.data.annotation.Id
import io.micronaut.data.annotation.MappedEntity

@MappedEntity
data class Article(
    val title: String = "",
    val text: String? = null
) {
    @Id
    @GeneratedValue
    val id: Int? = null
}