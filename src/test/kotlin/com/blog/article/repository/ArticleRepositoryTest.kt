package com.blog.article.repository

import com.blog.article.model.Article
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import org.junit.Assert
import org.junit.Rule
import org.junit.jupiter.api.Test
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.PostgreSQLR2DBCDatabaseContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import reactor.test.StepVerifier
import java.util.*
import javax.inject.Inject

@MicronautTest
class ArticleRepositoryTest {

    @Inject
    lateinit var articleRepository: ArticleRepository

    @Test
    fun `insert test`() {
        StepVerifier.create(
            articleRepository.save(
                Article(
                    title = "test",
                    text = UUID.randomUUID().toString()
                )
            )
        )
            .expectNextCount(1)
            .verifyComplete()

        StepVerifier.create(articleRepository.findAll())
            .assertNext {
                println(it)
                Assert.assertEquals("test", it.title)
            }
            .verifyComplete()
    }
}